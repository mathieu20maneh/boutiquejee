/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;


import java.util.ArrayList;
import java.util.List;
import entites.Produit;
/**
 *
 * @author mathi
 */
public class ProduitService {
    
     static List<Produit> liste = new ArrayList<>(); // à considérer comme la base de données des objets Cleint
   public void ajouter(Produit e){// ajoute l'objet e dans la collection liste
        liste.add(e);
    }
    public void modifier(Produit e){
        for(int i=0;i< liste.size();i++){
            if(e.getId() == liste.get(i).getId() ){
                liste.set(i, e);
            }
            }
    }// remplace par e, l'objet produit de la liste qui a même id que e
    public Produit trouver(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            return liste.get(i);
            }
        }
        return null;
    }// renvoie l'objet Produit de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            liste.remove(liste.get(i));
            }
        }
    }// retirer de la liste, l'objet Produit qui a l'id passé en paramètre
    public void supprimer(Produit e){
        liste.remove(e);
    }// retirer de la liste, l'objet Produit passé en paramètre
    public List<Produit> lister(){
        return liste;
    }// renvoyer tous les éléments de la liste
    public List<Produit> lister(int debut, int nombre){
        int size = debut+nombre>liste.size()?liste.size():debut+nombre; 
        List<Produit> list = new ArrayList<>();
        for(int i = debut; i < debut+nombre; i++){
            list.add(liste.get(i));
        }
        return list;
    }
    
}
