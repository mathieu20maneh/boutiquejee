/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import entites.Categorie;

/**
 *
 * @author mathi
 */
public class CategorieService {

    static List<Categorie> liste = new ArrayList<>(); // à considérer comme la base de données des objets Categorie
   public void ajouter(Categorie e){// ajoute l'objet e dans la collection liste
        liste.add(e);
    }
    public void modifier(Categorie e){
        for(int i=0;i< liste.size();i++){
            if(e.getId() == liste.get(i).getId() ){
                liste.set(i, e);
            }
            }
    }// remplace par e, l'objet Categorie de la liste qui a même id que e
    public Categorie trouver(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            return liste.get(i);
            }
        }
        return null;
    }// renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            liste.remove(liste.get(i));
            }
        }
    }// retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    public void supprimer(Categorie e){
        liste.remove(e);
    }// retirer de la liste, l'objet Categorie passé en paramètre
    public List<Categorie> lister(){
        return liste;
    }// renvoyer tous les éléments de la liste
    public List<Categorie> lister(int debut, int nombre){
        int size = debut+nombre>liste.size()?liste.size():debut+nombre; 
        List<Categorie> list = new ArrayList<>();
        for(int i = debut; i < debut+nombre; i++){
            list.add(liste.get(i));
        }
        return list;
    }// renvoyer nombre éléments de la liste, commençant à la position debut
}
