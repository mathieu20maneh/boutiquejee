/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import entites.Client;

/**
 *
 * @author mathi
 */
public class ClientService {

    static List<Client> liste = new ArrayList<>(); // à considérer comme la base de données des objets Cleint
   public void ajouter(Client e){// ajoute l'objet e dans la collection liste
        liste.add(e);
    }
    public void modifier(Client e){
        for(int i=0;i< liste.size();i++){
            if(e.getId() == liste.get(i).getId() ){
                liste.set(i, e);
            }
            }
    }// remplace par e, l'objet Client de la liste qui a même id que e
    public Client trouver(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            return liste.get(i);
            }
        }
        return null;
    }// renvoie l'objet Client de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            liste.remove(liste.get(i));
            }
        }
    }// retirer de la liste, l'objet Client qui a l'id passé en paramètre
    public void supprimer(Client e){
        liste.remove(e);
    }// retirer de la liste, l'objet Client passé en paramètre
    public List<Client> lister(){
        return liste;
    }// renvoyer tous les éléments de la liste
    public List<Client> lister(int debut, int nombre){
        int size = debut+nombre>liste.size()?liste.size():debut+nombre; 
        List<Client> list = new ArrayList<>();
        for(int i = debut; i < debut+nombre; i++){
            list.add(liste.get(i));
        }
        return list;
    }// renvoyer nombre éléments de la liste, commençant à la position debut
}

    

