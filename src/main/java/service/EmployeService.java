/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import entites.Employe;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mathi
 */
public class EmployeService {
     static List<Employe> liste = new ArrayList<>(); // à considérer comme la base de données des objets Cleint
   public void ajouter(Employe e){// ajoute l'objet e dans la collection liste
        liste.add(e);
    }
    public void modifier(Employe e){
        for(int i=0;i< liste.size();i++){
            if(e.getId() == liste.get(i).getId() ){
                liste.set(i, e);
            }
            }
    }// remplace par e, l'objet Employe de la liste qui a même id que e
    public Employe trouver(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            return liste.get(i);
            }
        }
        return null;
    }// renvoie l'objet Employe de la liste qui a l'id passé en paramètre
    public void supprimer(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            liste.remove(liste.get(i));
            }
        }
    }// retirer de la liste, l'objet Employe qui a l'id passé en paramètre
    public void supprimer(Employe e){
        liste.remove(e);
    }// retirer de la liste, l'objet Employe passé en paramètre
    public List<Employe> lister(){
        return liste;
    }// renvoyer tous les éléments de la liste
    public List<Employe> lister(int debut, int nombre){
        int size = debut+nombre>liste.size()?liste.size():debut+nombre; 
        List<Employe> list = new ArrayList<>();
        for(int i = debut; i < debut+nombre; i++){
            list.add(liste.get(i));
        }
        return list;
    }
}
