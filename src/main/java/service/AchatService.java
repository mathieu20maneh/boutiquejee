/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import entites.Achat;


/**
 *
 * @author mathi
 */
public class AchatService {
    static List<Achat> liste = new ArrayList<>();
    
    public void ajouter(Achat e){// ajoute l'objet e dans la collection liste
        liste.add(e);
    }
    
    public void modifier(Achat e){
        for(int i=0;i< liste.size();i++){
            if(e.getId() == liste.get(i).getId() ){
                liste.set(i, e);
            }
            }
    }
    
    public Achat trouver(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            return liste.get(i);
            }
        }
        return null;
    }
    
     public void supprimer(Integer id){
        for(int i= 0;i<liste.size();i++){
            if(liste.get(i).getId() ==  id ){
            liste.remove(liste.get(i));
            }
        }
    }
     
     public List<Achat> lister(){
        return liste;
    }
     
     public List<Achat> lister(int debut, int nombre){
        int size = debut+nombre>liste.size()?liste.size():debut+nombre; 
        List<Achat> list = new ArrayList<>();
        for(int i = debut; i < debut+nombre; i++){
            list.add(liste.get(i));
        }
        return list;
    }
}
