/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Achat;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.AchatService;

/**
 *
 * @author mathi
 */
@Path("/achat")
public class AchatResource {
    AchatService achatService = new AchatService();
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Achat trouver(@PathParam("id") Integer id){
        return achatService.trouver(id);
    }// renvoie l'objet Achat de la liste qui a l'id passé en paramètre
    
    @GET
    @Path("/liste")
    @Produces(MediaType.APPLICATION_JSON)        
    public List<Achat> lister(){
        return achatService.lister();
    }// renvoyer tous les éléments de la liste
    
    @GET
    @Path("/lister")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> lister(@QueryParam("debut")int debut,@QueryParam("nombre")int nombre){
        return achatService.lister(debut,nombre);
    }// renvoyer nombre éléments de la liste, commençant à la position debut
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Achat e){
        achatService.ajouter(e);
    }// ajoute l'objet e dans la collection liste
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Achat e){
        achatService.modifier(e);
    }// remplace par e, l'objet Achat de la liste qui a même id que e
    
    @DELETE
    @Path("/delete/{achatId}")
    public void supprimer(@PathParam("achatId")Integer id){
        achatService.supprimer(id);
    }// retirer de la liste, l'objet Achat qui a l'id passé en paramètre
    
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Achat e){
        achatService.supprimer(e);
    }
}
