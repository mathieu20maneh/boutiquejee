/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Employe;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.EmployeService;

/**
 *
 * @author mathi
 */
public class EmployeResource {
    
EmployeService employeService = new EmployeService();
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Employe trouver(@PathParam("id") Integer id){
        return employeService.trouver(id);
    }// renvoie l'objet Employe de la liste qui a l'id passé en paramètre
    
    @GET
    @Path("/liste")
    @Produces(MediaType.APPLICATION_JSON)        
    public List<Employe> lister(){
        return employeService.lister();
    }// renvoyer tous les éléments de la liste
    
    @GET
    @Path("/lister")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> lister(@QueryParam("debut")int debut,@QueryParam("nombre")int nombre){
        return employeService.lister(debut,nombre);
    }// renvoyer nombre éléments de la liste, commençant à la position debut
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Employe e){
        employeService.ajouter(e);
    }// ajoute l'objet e dans la collection liste
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Employe e){
        employeService.modifier(e);
    }// remplace par e, l'objet Employe de la liste qui a même id que e
    
    @DELETE
    @Path("/delete/{EmployeId}")
    public void supprimer(@PathParam("EmplyeId")Integer id){
        employeService.supprimer(id);
    }// retirer de la liste, l'objet Employe qui a l'id passé en paramètre
    
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Employe e){
        employeService.supprimer(e);
    }
}
