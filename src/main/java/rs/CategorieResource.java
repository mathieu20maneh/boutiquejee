/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.CategorieService;

/**
 *
 * @author JIX
 */
@Path("/categorie")
public class CategorieResource {
    
    CategorieService categorieService = new CategorieService();
    
    //@GET
    //@Path("/hello")
    //public String salut(){
     //   return "salut";
    //}
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categorie trouver(@PathParam("id") Integer id){
        return categorieService.trouver(id);
    }// renvoie l'objet Categorie de la liste qui a l'id passé en paramètre
    
    @GET
    @Path("/liste")
    @Produces(MediaType.APPLICATION_JSON)        
    public List<Categorie> lister(){
        return categorieService.lister();
    }// renvoyer tous les éléments de la liste
    
    @GET
    @Path("/lister")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> lister(@QueryParam("debut")int debut,@QueryParam("nombre")int nombre){
        return categorieService.lister(debut,nombre);
    }// renvoyer nombre éléments de la liste, commençant à la position debut
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter(Categorie e){
        categorieService.ajouter(e);
    }// ajoute l'objet e dans la collection liste
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Categorie e){
        categorieService.modifier(e);
    }// remplace par e, l'objet Categorie de la liste qui a même id que e
    
    @DELETE
    @Path("/delete/{CategorieId}")
    public void supprimer(@PathParam("CategorieId")Integer id){
        categorieService.supprimer(id);
    }// retirer de la liste, l'objet Categorie qui a l'id passé en paramètre
    
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Categorie e){
        categorieService.supprimer(e);
    }// retirer de la liste, l'objet Categorie passé en paramètre
}
