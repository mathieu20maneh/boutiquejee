/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import entites.Produit;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitService;

/**
 *
 * @author mathi
 */
@Path("/produit")

public class ProduitResource {
    
    ProduitService produitService = new ProduitService();
 
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Produit trouver(@PathParam("id") Integer id){
        return produitService.trouver(id);
    }
    
    @GET
    @Path("/liste")
    @Produces(MediaType.APPLICATION_JSON)        
    public List<Produit> lister(){
        return produitService.lister();
    }// renvoyer tous les éléments de la liste
    
    @GET
    @Path("/lister")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> lister(@QueryParam("debut")int debut,@QueryParam("nombre")int nombre){
        return produitService.lister(debut,nombre);
    }// renvoyer nombre éléments de la liste, commençant à la position debut
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void ajouter( Produit e){
        produitService.ajouter(e);
    }// ajoute l'objet e dans la collection liste
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifier(Produit e){
        produitService.modifier(e);
    }
    
    @DELETE
    @Path("/delete/{produitId}")
    public void supprimer(@PathParam("ProduitId")Integer id){
        produitService.supprimer(id);
    }
    
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimer(Produit e){
        produitService.supprimer(e);
    }
}
