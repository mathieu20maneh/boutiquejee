/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Mathieu Maneh
 */
public class Principal {
    public static void main( String args[]){
        Personne pers = new Personne(0, "KPOTRO", "Kadjan", LocalDate.MIN);
        Client client = new Client ("009828733",0, "KOPOUYTR", "Kadjanus ", LocalDate.MIN);
        Categorie cat1,cat2;
        Produit produit1,produit2,produit3;
        ProduitAchete buyProd1, buyProd2;
        Achat achat1;
        ArrayList<ProduitAchete> ListeProduits;
        
        cat1 = new Categorie(0, "ELectro", "Pour gérér les produits electro menager");
        cat2 = new Categorie(0, "Informatique", "Pour gérér les produits Informatique ");
       
        produit1 = new Produit(0, "Ampoule Electrique Manager", 1200, LocalDate.now(), cat1);
        produit2 = new Produit(0, "Computer", 1400, LocalDate.parse("2012-02-17"),cat2);
        
        buyProd1=new ProduitAchete(5, 3, produit1);
        buyProd2=new ProduitAchete(5, 3, produit2);
        ListeProduits = new ArrayList();
        ListeProduits.add(buyProd1);
        boolean add = ListeProduits.add(buyProd2);
        achat1 = new Achat(1, 24,LocalDate.parse("2015-02-17"), ListeProduits, pers);
        
        System.out.println(achat1);
    }
    
}
